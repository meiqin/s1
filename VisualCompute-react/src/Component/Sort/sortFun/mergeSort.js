export default function(arr,showSort){
    //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    //一次归并算法
    var merge = async function(out, array, s, m, t){
        let i=s, j=m+1, k=s;//i:左边数组的索引，j:右边数组的索引，k:归并结果数组的索引
        //没有数组遍历完
        while(i<=m && j<=t){
            if(out[i]<out[j]){
                showSort(array,i,j);
                array[k++] = out[i++];
                showSort(array,k-1,k-1);
            }else{
                showSort(array,i,j);
                array[k++] = out[j++];
                showSort(array,k-1,k-1);
            }
        }
        //如果左数组没有遍历完，将左数组剩余数据压入arr
        if(i<=m){
            while(i<=m){
                if(typeof showSort === "function"){
                    showSort(array,null,i);
                }
                array[k++] = out[i++];
                if(typeof showSort === "function"){
                    showSort(array,null,null);
                }
            }
        }else{
            while(j<=t){
                if(typeof showSort === "function"){
                    showSort(array,null,j);
                }
                array[k++] = out[j++];
                if(typeof showSort === "function"){
                     showSort(array,k-1,k-1);
                }
            }
        }
        return array;
    }
    //一趟归并排序算法
    var mergePass =async function(out, array, h){
        var len = out.length;
        let i = 0;
        while(i+2*h<=len){
            merge(out, array, i , i+h-1, i+2*h-1);
            i += 2*h;
        }
        if(i+h<len){
             merge(out, array, i, i+h-1, len-1);
        }else{
            while(i<len){
                array[i] = out[i];
                i++;
            }
        }
        //console.log(arr);
        return array;
    }
    //非递归归并排序
    var mergeSortUnrecursion = async function(out){
        var len = out.length;
        var array = [];
        for(let i=0;i<arr.length;i++){
            array[i] = out[i];
        }
        var h = 1;
        while(h<len){
            mergePass(out, array, h);
            h = 2*h;
            mergePass(array, out, h);
            h = 2*h;
        }
        //console.log(out);
        return out;
    }
    //递归归并排序
    var mergeSortRecursion = async function(out,array, s, t){
        if(s === t){
            array[s] = out[s];
        }else{
            let m = parseInt((s+t)/2);
            mergeSortRecursion(out, array, s, m);
            mergeSortRecursion(out, array, m+1, t);
            merge(array, out, s, m, t);
            //将out复制给array,继续下一轮归并
            for(let i=0;i<out.length;i++){
                array[i] = out[i];
            }
        }
        return out;
    }
    var array = [];
    return mergeSortUnrecursion(out,array, 0, arr.length-1);
}
