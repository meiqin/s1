
export default function(arr,showSort){
    //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    //快速排序
    var quick = async function(out,first,end){
        if(first < end){
            let i = first, j = end, temp = 0;
            while(i<j){
                //j--
                while(i < j && out[i] < out[j]){
                     showSort(out,i,j);                   
                    j--;
                }
                //交换位置，i++
                if(i<j){
                    showSort(out,i,j);
                    temp = out[i];
                    out[i] = out[j];
                    out[j] = temp;
                    showSort(out,null,null)
                    i++;
                }
                //i++
                while(i < j && out[i] < out[j]){
                    showSort(out,j,i);
                    i++;
                }
                //交换位置，j--
                if(i<j){
                    showSort(out,j,i);
                    temp = out[i];
                    out[i] = out[j];
                    out[j] = temp;
                    showSort(out,null,null)
                    j--;
                }
            }
            await showSort(out,i,i);
            quick(out,first,i-1);
            quick(out,i+1,end);
        }
        return out;
    }
    return quick(out,0,out.length-1);
}