
export default function(arr,showSort){
    //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    var len = out.length;
    //建立堆
    var sift = async function(out, k, m){
        let i = k, j = 2*k+1;
        while(j <= m && j!==len){
            if(j<m && out[j+1] && out[j]<out[j+1]){
                j++;
            }
            if(out[i] > out[j]){
                break;
            }else{
                showSort(out,i,j);
                let temp = out[i];
                out[i] = out[j];
                out[j] = temp;
                showSort(out,null,null);              
                i = j;
                j = 2*i+1;
            }
        }
    }

    let half = parseInt(len/2);
    //初始建堆
    for(let i=half-1;i>=0;i--){
        sift(out, i, len);
    }
    for(let i=0;i<len-1;i++){
        showSort(out,0,len-1-i);
        let temp = out[0];
        out[0] = out[len-1-i];
        out[len-1-i] = temp;
        showSort(out,null,null);     
        sift(out, 0, len-1-i-1);
    }
    return out;
}