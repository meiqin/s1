
import bubbleSort from './bubbleSort';
import bucketSort from './bucketSort';
import heapSort from './heapSort';
import mergeSort from './mergeSort';
import quickSort from './quickSort';
import redixSort from './redixSort';
import selectSort from './selectSort';
import shellSort from './shellSort';
import insertSort from './insertSort';
import countSort from './countSort'

export default{
    bubbleSort,
    heapSort,
    bucketSort,
    mergeSort,
    quickSort,
    redixSort,
    selectSort,
    shellSort,
    insertSort,
    countSort,
}
