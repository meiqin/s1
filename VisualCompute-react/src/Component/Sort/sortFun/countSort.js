export default function(arr,showSort){
    //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    var len = out.length,
    max = out[0];
    //创建长度max的数组，填充0
    var C= [];
    for (var i = 0; i < len; i++) {　
         max = max >=  out[i] ? max : out[i];
         C[out[i]] =  C[out[i]] ?  C[out[i]]+1 : 1;   //遍历输入数组。填充C
    }

    var sortedIndex = 0;
    //遍历C，输出数组
    for(var k = 0;k <= max; k++){
        while(C[k]-- >0){            // 按顺序将值推入输出数组，并在比较后将对应标志位减1
            out[sortedIndex] = k;
            showSort(out,sortedIndex,null);
            sortedIndex ++; 
        }
    }  
}