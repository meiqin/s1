
export default async function(arr,showSort){
    //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    var half = parseInt(out.length/2);    //动态定义间隔序列
    for(let d = half; d >= 1;d = parseInt(d/2)){       //一个一个间隔值开始
        for(let i = d;i< out.length;i++){              //以间隔值遍历
            for(let j=i-d;j >=0;j-=d){
                await showSort(out,j,j+d);
                if(out[j+d] < out[j]){
                    let tem = out[j+d];
                    out[j+d] = out[j];
                    out[j] = tem;
                }
                await showSort(out,null,null);
            }
        }
    }
    return out;
}