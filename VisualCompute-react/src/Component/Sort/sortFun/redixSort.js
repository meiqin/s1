
export default function(arr,showSort){
    //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    var len = out.length;
    //求所有数中最大的
    var max = 0;
    for(let i=0;i<len;i++){
        if(out[i]>max){
            max = out[i];
        }
    }
    //计算所有数中最大的是几位数
    var max_pow = 1;
    while(max>=10){
        max_pow++;
        max = parseInt(max/10);
    }
    var distributeUp = function(out, queue, pow){
        queue.splice(0,queue.length);
        for(let i=0;i<len;i++){
            let m = parseInt(out[i]/pow)%10;
            if(Object.prototype.toString.call(queue[m]) !== "[object Array]"){
                queue[m] = [];
            }
            queue[m].push(out[i]);
            showSort(queue,m,i);
        }
    }
    var collectUp = function(out, queue){
        out.splice(0,out.length);
        for(let i=0;i<10;i++){
            while(queue[i]!==undefined && queue[i].length>0){
                out.push(queue[i].shift());
                showSort(out,i,null);
            }
        }
    }
    var queue = []; 
    for(let i=0;i<max_pow;i++){
        distributeUp(out, queue, Math.pow(10,i));
        collectUp(out, queue);
    }
    showSort(out,null,null);
    return out;
}