
export default async function(arr,showSort){
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    for(let i=0;i<out.length;i++){
        let index = i;
        for(let j=i;j<out.length;j++){
           await showSort(out,index,j);
            if(out[j]<out[index]){
                index = j;
            }
        }
        let temp = out[i];
        out[i] = out[index];
        out[index] = temp;
        await showSort(out,null,null);
    }
    return out;
}