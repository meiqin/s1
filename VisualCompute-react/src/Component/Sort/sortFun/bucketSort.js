
export default function (arr,showSort) {
    var data = [];
    for(let i=0;i<arr.length;i++){
        data[i] = arr[i];
    }
    //桶的初始化  
    var DEFAULT_BUCKET_SIZE = 5;            //设置桶的默认数量为5
    var bucketSize =  DEFAULT_BUCKET_SIZE; 
    if (data.length === 0) {
        return data;
    }
    var len = data.length, buckets = [], result = [], min =data[0], max = data[0], space, n = 0;     
    var index = Math.floor(len / bucketSize) ;
    while(index<2){       
        bucketSize--;
        index = Math.floor(len / bucketSize) ;
    }     
    //找出一组书中的最大值和最小值
    for (var i = 1; i < len; i++) {
        min = min <= data[i] ? min : data[i];     //一组数据中的最小值
        max = max >= data[i] ? max : data[i];     //一组数据中的最小值
    }
    space = (max - min + 1) / bucketSize;             //平均每个桶的范围
    for (var j = 0; j < len; j++) {
        var index1 = Math.floor((data[j] - min) / space);           //看data[j]属于哪个桶
        if (buckets[index1]) {                                        // 非空桶，插入排序
         　  var k = buckets[index1].length - 1;   
        　　 while (k >= 0 && buckets[index1][k] > data[j]) {        //第index1个桶的第k个数
        　　　　 buckets[index1][k + 1] = buckets[index1][k];
        　　　　 k--;
        　 　}
        　　 buckets[index1][k + 1] = data[j];
        } else {                                                 //空桶，初始化
         　　 buckets[index1] = [];
        　　　buckets[index1].push(data[j]);
        }
    }
    while (n < bucketSize) {
        result = result.concat(buckets[n]);
        n++;
    }
     showSort(result);  
    return result;
}