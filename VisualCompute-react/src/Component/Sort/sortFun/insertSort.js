
export default function (arr,showSort){
        //深拷贝，不改动输入数组
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    for(let i=1;i<out.length;i++){
        let temp = out[i];
        for(var j=i-1;j>=0;j--){
            if(out[j] > temp){
                out[j+1] = out[j];
                out[j] = temp;
                showSort(out,j,null);                   
            }else{
                break;    //找到比temp小的则跳出循环
            }
        }
        out[j+1] = temp;   //在比temp小的值后面插入temp值
        showSort(out,j+1,j+1);
    }
    return out;
}