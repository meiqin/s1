

export default  function (arr,showSort){
    //冒泡排序, arr:Array ,showSort:排序可视化
    var out = [];
    for(let i=0;i<arr.length;i++){
        out[i] = arr[i];
    }
    for(let i=1;i<out.length;i++){
        for(let j=0;j<out.length-i;j++){
            showSort(out,j,j+1);
            if(out[j] > out[j+1]){
                let tem = out[j];
                out[j] = out[j+1];
                out[j+1] = tem;
            }
            showSort(out,null,null);   
        }
    }
    return out;
}