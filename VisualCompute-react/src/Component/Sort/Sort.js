import React from 'react';
import sortFun from './sortFun/index';

function getSortData(n){
    var arr = [], i=0;
    while(i++<n){
        let v = Math.floor((Math.random()*90+10));//10~100
        arr.push(v);
    }
    return arr;
}

var show_count = 0
var deley_count = 0
var timeList = []

class Sort extends React.Component{
    constructor(props){
        super(props);
        let initData = getSortData(10);
        this.state = {
            data: initData,
            process: false
        };
    }
    render(){
        return (
            <div className="container">
                <div className="control">
                    <label>设置数组长度:</label>
                    <input type="number" id="number"/>
                        <select id="select">
                            <option value="bubbleSort">冒泡排序</option>
                            <option value="insertSort">插入排序</option>
                            <option value="shellSort">希尔排序</option>
                            <option value="quickSort">快速排序</option>
                            <option value="selectSort">选择排序</option>
                            <option value="heapSort">堆排序</option>
                            <option value="mergeSort">归并排序</option>
                            <option value="bucketSort">桶排序</option>
                            <option value="countSort">计数排序</option>
                            <option value="redixSort">基数排序</option>
                         </select>
                    <label>请点击开始</label>
                    <button className="btn" onClick={this.handleStart.bind(this)}>开始</button>
                    <button className="btn" onClick={this.handleReset.bind(this)}>重置</button>
                </div>
                <svg id="svg" width={800} height={500}></svg>
            </div>
        );}
    handleReset(){
        for(var i=0;i<timeList.length;i++){ 
            clearInterval(timeList[i]);
            show_count = 0
        }
        var n = document.getElementById("number").value 
        if(!n){n=10}
        let initData = getSortData(n)
        showArr(initData)  
        this.setState( (prevState) => {return {
            data: initData,
            process : false,
            deley_count : 0 
        }});

    }
    handleStart(){
        this.setState((prevState)=>{return {
            process : true
        }});
        var select = document.getElementById("select").value;
        if(!sortFun.hasOwnProperty(select)){
            alert("无该排序方法");
            return;
        }
        var data = this.state.data.slice();
        var iter = sortFun[select](data,this.showSort);  
		var traversalArr = [];
		//按选择的排序方式和顺序排序
		switch(select){
			case "bubbleSort": traversalArr = iter;
					console.log("冒泡排序1111:",traversalArr);
					break;
			case "insertSort": traversalArr = iter;
					console.log("插入排序:",traversalArr);
					break;
			case "shellSort": traversalArr = iter;
					console.log("希尔排序:",traversalArr);
					break;
			case "quickSort": traversalArr = iter;
					console.log("快速排序:",traversalArr);
					break;
			case "selectSort": traversalArr = iter;
					console.log("选择排序:",traversalArr);
					break;
			case "heapSort": traversalArr = iter;
					console.log("堆排序:",traversalArr);
					break;
			case "mergeSort": traversalArr = iter;
					console.log("归并排序:",traversalArr);
					break;
			case "bucketSort": traversalArr = iter;
					console.log("桶排序:",traversalArr);
                    break;
            case "countSort": traversalArr = iter;
					console.log("计数排序:",traversalArr);
					break;
			case "redixSort": traversalArr = iter;
					console.log("基数排序:",traversalArr);
					break;
			default: //alert("选择遍历方式出错");
					break;
		}	  
    }
    /************处理各种算法**************/
    showSort = (arr,m,n) => {
        var array = [];
        for(let i=0;i<arr.length;i++){
            array[i] = arr[i];
        }
        var callback =(array,m,n) => {
            if (!this.state.process) return
            deley_count = ++deley_count
            showArr(array,m,n);
            if(deley_count >= show_count){
                setTimeout(function(){
                    showArr(array);
                    show_count = 0;
                    deley_count = 0;
                },100);
            }
        }
        var time = 500;
        timeList[show_count] = setTimeout(callback,time*show_count++,array,m,n);
    }
}

function showArr(arr,m,n){
	var svg = document.getElementById("svg");
	var rectStr = "",textStr = "",lineStr = "";//矩形、文本和线的HTML字符串
	var height = svg.getAttribute("height"),width = svg.getAttribute("width");//画布宽高
	var rectWidth = 0, rectHeight = 0, spaceWidth = 0;//矩形宽度、间隔宽度
	var margin_level = 20,margin_veticle = 40;//水平、垂直边距
	var maxValue = 0;
	rectWidth = (width-margin_level*2)/(arr.length)*0.6;
	spaceWidth = rectWidth*2/3;
	svg.innerHTML = "";
 
	var getMax = function(arr){
		var max = 0;
		for(let i=0;i<arr.length;i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}
		return max;
	}
	var getHeight = function(h){
		return (height-2*margin_veticle)*(h/maxValue);
	}
	maxValue = getMax(arr);
	
	//画线和数字
	for(let i=0;i<arr.length;i++){
		let cx = 0, cy = 0;//当前结点的定位像素坐标
		let color = "#5CACEE";
		rectHeight = getHeight(arr[i]);
		cx = i * (spaceWidth + rectWidth) + margin_level;
		cy = height - rectHeight - margin_veticle;
 
		if(i=== m){
			color = "#FF3E96";
		}else if(i=== n){
			color = "#7CCD7C";
		}
		if(i===m && i===n){
			color = "#EEC900";
		}
		var font_size = rectWidth/2>20?20:rectWidth/2
		rectStr += '<rect x="'+cx+'" y="'+cy+'" width="'+rectWidth+'" height="'+rectHeight+'" fill="'+color+'"/>';
		textStr += '<text x="'+(cx+rectWidth/2)+'" y="'+(cy-6)+'" fill="#171717"  style="font-size:'+font_size+'pt;text-anchor: middle">'+arr[i]+'</text>';
		textStr += '<text x="'+(cx+rectWidth/2)+'" y="'+(height-25+font_size)+'" fill="#171717"  style="font-size:'+font_size+'pt;text-anchor: middle">'+(i+1)+'</text>';
	}
	lineStr += '<line x1="0" y1="0" x2="0" y2="'+(height-30)+'" style="stroke:#666;stroke-width:4" />'
	 +'<line x1="0" y1="'+(height-margin_veticle+10)+'" x2="'+width+'" y2="'+(height-margin_veticle+10)+'" style="stroke:#666;stroke-width:2" />';
	svg.innerHTML = lineStr+rectStr+textStr;
}

export {Sort as default}