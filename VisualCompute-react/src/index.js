import 'jquery'
import React from 'react';
import ReactDOM from 'react-dom';
//import MainPage from "./VisualList";
import Sort from "./Component/Sort/Sort"
import './Styles/style.css'
import {BrowserRouter as Router} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Router>
      <Sort/>
    </Router>, 
     document.getElementById('root')
 );
serviceWorker.unregister();
